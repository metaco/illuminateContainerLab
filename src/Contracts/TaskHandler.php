<?php
/**
 *
 */

namespace App\Contracts;


/**
 * Interface TaskHandler
 * @package App\Contracts
 */
interface TaskHandler
{
    /**
     * @param string $taskName
     */
    public function setTaskName(string $taskName): void;

    /**
     *
     */
    public function run() : void;
}