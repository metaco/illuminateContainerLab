<?php
/**
 *
 */

namespace App\Task;


use App\Contracts\TaskHandler;

class CrashRecordHandler implements TaskHandler
{

    public function setTaskName(string $taskName): void
    {
        dump("taskName : {$taskName} has been set!");
    }

    public function run(): void
    {
        dump('task : crash record run ...');
    }
}