<?php
/**
 *
 */

namespace App\Client;


class GitHub
{
    protected $url = 'default';

    public function setEnterpriseUrl(string $url)
    {
        $this->url = $url;
    }

    public function requestEnterpriseURL()
    {
        $curlClient = curl_init();

        curl_setopt($curlClient, CURLOPT_URL, 'http://github.com');
        curl_setopt($curlClient, CURLOPT_POST, []);
        curl_setopt($curlClient, CURLOPT_RETURNTRANSFER, 1);

        $data = curl_exec($curlClient);
        echo $data;
    }
}