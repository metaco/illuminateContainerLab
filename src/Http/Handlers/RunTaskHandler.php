<?php
/**
 *
 */

namespace App\Http\Handlers;


use App\Contracts\TaskHandler;

class RunTaskHandler
{
    protected $task;

    /**
     * RunTaskHandler constructor.
     * @param $task
     */
    public function __construct(TaskHandler $task)
    {
        $this->task = $task;
    }

    public function runTask(string $taskName)
    {
        $this->task->setTaskName($taskName);
        $this->task->run();
    }
}