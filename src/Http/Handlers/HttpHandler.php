<?php
/**
 *
 */

namespace App\Http\Handlers;


use App\Http\Request;

class HttpHandler
{
    protected $request;

    /**
     * HttpHandler constructor.
     * @param $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function response(string $request, string $response)
    {
        $this->request->getResponse($request, $response);
    }
}