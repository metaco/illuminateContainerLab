<?php
/**
 *
 */

namespace App\Http\Controllers;


use App\Http\Request;

class HomeController
{
    protected $request;

    /**
     * HomeController constructor.
     * @param $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index(int $id)
    {
        dump("index method :{$id}");
    }
}