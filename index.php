<?php
/**
 *
 */

require_once __DIR__  . '/vendor/autoload.php';

use Illuminate\Container\Container;
use App\Http\Handlers\HttpHandler;
use App\Client\GitHub;
use App\Http\Controllers\HomeController;


$container = Container::getInstance();


$httpHandler = $container->make(HttpHandler::class);
$httpHandler->response('HTTP/1.1 200 OK ', 'Content-Type: application/json');

$container->bind(\App\Contracts\TaskHandler::class, \App\Task\CrashRecordHandler::class);
$runTaskHandler = $container->make(\App\Http\Handlers\RunTaskHandler::class);

$runTaskHandler->runTask('set crash');


 // 会在绑定的类解析完成之后调用。
$container->resolving(GitHub::class, function (GitHub $client) {
    $client->setEnterpriseUrl('http://github.com/enterprise');
});

// 在这里完成 resolving
$client = $container->make(GitHub::class);
$client->requestEnterpriseURL();


$homeCtrl = $container->make(HomeController::class);
//$container->call([$homeCtrl, 'index'], ['id' => 10]);
$container->call('\App\Http\Controllers\HomeController@index', ['id' => 10]);